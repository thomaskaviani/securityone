package service.remotes;

import domain.User;
import service.generics.GenericServiceRemote;

import java.util.List;

public interface UserServiceRemote extends GenericServiceRemote<User> {

    User findById(Long id);

    List<User> findAll();

    User findByName(String name);
}
