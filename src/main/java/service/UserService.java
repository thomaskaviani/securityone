package service;

import domain.User;
import repository.UserRepository;
import service.generics.GenericService;
import service.remotes.UserServiceRemote;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@Remote
@LocalBean
public class UserService extends GenericService<User> implements UserServiceRemote {

    @Inject
    UserRepository userRepository;

    @PostConstruct
    public void init(){
        setGenericRepository(userRepository);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findByName(String name) {
        return userRepository.findByName(name);
    }
}
