package service.generics;

public interface GenericServiceRemote<T> {

    T save(T obj);
    void remove(T obj);
    T update(T obj);

}
