package service.generics;

import repository.GenericRepository;

public class GenericService<T> implements GenericServiceRemote<T> {

    private GenericRepository<T> genericRepository;

    @Override
    public T save(T obj) {
        genericRepository.save(obj);
        return obj;
    }

    @Override
    public void remove(T obj) {
        genericRepository.remove(obj);
    }

    @Override
    public T update(T obj) {
        return genericRepository.update(obj);
    }

    public GenericRepository<T> getGenericRepository() {
        return genericRepository;
    }

    public void setGenericRepository(GenericRepository<T> genericRepository) {
        this.genericRepository = genericRepository;
    }
}
