package repository;

import domain.User;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class UserRepository extends GenericRepository<User> {

    public User findById(Long id){
        return entityManager.find(User.class, id);
    }

    public List<User> findAll(){
        return entityManager.createNamedQuery(User.FIND_ALL, User.class)
                .getResultList();
    }

    public User findByName(String name){
        try {
            return entityManager.createNamedQuery(User.FIND_BY_NAME, User.class)
                    .setParameter("myName", name)
                    .getSingleResult();
        } catch (Exception e){
            return null;
        }

    }

}
