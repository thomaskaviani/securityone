package repository;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class EntityManagerProducer {

    @PersistenceContext(name = "SecurityOnePU")
    EntityManager entityManager;

    @Produces
    public EntityManager entityManager(){
        return entityManager;
    }

}
