package repository;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class GenericRepository<T> {

    @Inject
    EntityManager entityManager;

    public T save(T obj){
        entityManager.persist(obj);
        return obj;
    }

    public T update(T obj){
        return entityManager.merge(obj);
    }

    public void remove(T obj){
        entityManager.remove(entityManager.contains(obj) ? obj : entityManager.merge(obj));
    }

}
