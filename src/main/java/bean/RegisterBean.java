package bean;

import domain.User;
import org.omnifaces.cdi.ViewScoped;
import service.UserService;
import util.Encrypt;
import util.Endpoints;
import util.Navigation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

@Named
@ViewScoped
public class RegisterBean implements Serializable {

    @NotEmpty
    @Size(min = 4, message = "Password must have at least 4 characters")
    private String password;

    @NotEmpty
    @Size(min = 4, message = "Username must have at least 4 characters")
    private String username;

    @Inject
    UserService userService;

    public String register() throws NoSuchAlgorithmException {

        byte[] salt = Encrypt.getSalt();
        String securePassword = Encrypt.get_SHA_512_SecurePassword(password, salt);

        User u = new User();
        u.setUsername(username);
        u.setSalt(salt);
        u.setPassword(securePassword);

        userService.save(u);

        return Navigation.redirect(Endpoints.INDEX_PAGE).build();
    }




    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
