package bean;

import util.Endpoints;
import util.Navigation;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class NavigationBean implements Serializable {

    public static String toLogin(){
        return Navigation.redirect(Endpoints.LOGIN_PAGE).build();
    }

    public static String toRegister(){
        return Navigation.redirect(Endpoints.REGISTER_PAGE).build();
    }

    public static String toIndex(){
        return Navigation.redirect(Endpoints.INDEX_PAGE).build();
    }

    public static String toUnsecure(){
        return Navigation.redirect(Endpoints.UNSECURE_PAGE).build();
    }

    public static String toSecure(){
        return Navigation.redirect(Endpoints.SECURE_PAGE).build();
    }
}
