package util;

public class Navigation {

    private String url;
    private boolean hasParam;

    private Navigation(String url){
        hasParam = false;
        this.url = url;
    }

    public static Navigation redirect(String url){
        return new Navigation(url);
    }

    public Navigation param(String paramName, String param){
        addParam();
        url += paramName + "=" + param;
        return this;
    }

    public Navigation param(String paramName, Long param){
        addParam();
        url += paramName + "=" + param;
        return this;
    }

    public String build(){
        addParam();
        return url + "faces-redirect=true";
    }

    private void addParam(){
        if (!hasParam){
            url += "?";
            hasParam = true;
        } else {
            url += "&";
        }
    }
}
