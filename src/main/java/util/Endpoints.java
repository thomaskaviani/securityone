package util;


public class Endpoints {

    public static final String INDEX_PAGE = "/app/index.xhtml";

    public static final String LOGIN_PAGE = "/login.xhtml";
    public static final String REGISTER_PAGE = "/register.xhtml";
    public static final String ERROR_PAGE = "/login-error.xhtml";

    public static final String SECURE_PAGE = "/app/secure/secure.xhtml";
    public static final String UNSECURE_PAGE = "/app/unsecure.xhtml";

}
