package util;

import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

import javax.servlet.ServletContext;

@RewriteConfiguration
public class RewriteConfig extends HttpConfigurationProvider {

    @Override
    public Configuration getConfiguration(ServletContext servletContext) {
        return ConfigurationBuilder.begin()

                .addRule(Join.path("")
                        .to(Endpoints.INDEX_PAGE))

                .addRule(Join.path("/")
                        .to(Endpoints.INDEX_PAGE))

                .addRule(Join.path("/login")
                        .to(Endpoints.LOGIN_PAGE))

                .addRule(Join.path("/register")
                        .to(Endpoints.REGISTER_PAGE))

                .addRule(Join.path("/login-error")
                        .to(Endpoints.ERROR_PAGE))

                .addRule(Join.path("/secure")
                        .to(Endpoints.SECURE_PAGE))

                .addRule(Join.path("/unsecure")
                        .to(Endpoints.UNSECURE_PAGE))

                ;
    }

    @Override
    public int priority() {
        return 10;
    }
}
