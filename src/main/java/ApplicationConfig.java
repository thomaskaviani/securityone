import util.Endpoints;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.inject.Named;
import javax.security.enterprise.authentication.mechanism.http.CustomFormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;

@CustomFormAuthenticationMechanismDefinition(
        loginToContinue = @LoginToContinue(
                loginPage = Endpoints.LOGIN_PAGE,
                useForwardToLogin = false
        )
)
@FacesConfig
@Named
@ApplicationScoped
public class ApplicationConfig {

}
