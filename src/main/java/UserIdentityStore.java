import domain.User;
import service.UserService;
import util.Encrypt;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.Arrays;
import java.util.HashSet;

@ApplicationScoped
public class UserIdentityStore implements IdentityStore {

    @Inject
    UserService userService;

    @Override
    public CredentialValidationResult validate(Credential credential) {

        UsernamePasswordCredential login = (UsernamePasswordCredential) credential;

        User u = userService.findByName(login.getCaller());

        if (u != null && u.getSalt() != null){

            String securePassword = Encrypt.get_SHA_512_SecurePassword(login.getPasswordAsString(), u.getSalt());

            if (u.getUsername().equals("admin") && securePassword.equals(u.getPassword())){
                return new CredentialValidationResult(login.getCaller(), new HashSet<>(Arrays.asList("ADMIN")));
            } else if (securePassword.equals(u.getPassword())){
                return new CredentialValidationResult(login.getCaller(), new HashSet<>(Arrays.asList("USER")));
            } else {
                return CredentialValidationResult.NOT_VALIDATED_RESULT;
            }
        } else {
            return CredentialValidationResult.NOT_VALIDATED_RESULT;
        }

    }


}